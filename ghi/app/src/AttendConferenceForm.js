import React, { useState, useEffect } from 'react';


function AttendConferenceForm () {

    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [hasSignedUp, setHasSignedUp] = useState(false);


    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }


    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.email = email;
        data.name = name;
        data.conference = conference;


        const jsonData = JSON.stringify(data);

        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: jsonData,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                // const newAttendee = await response.json();

                setName('');
                setEmail('');
                setConference('');
                setHasSignedUp(true);
            }
    }


    // CSS classes for rendering
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }


    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="my-5 container">
            <div className="row">

                <div className=" col-3">
                    <div className="shadow p-0 mt-4">
                        <img src="/logo.svg" width="260" height="180" />
                    </div>
                </div>

                <div className="col-9">
                    <div className="shadow p-4 mt-4">
                        <h1>It's Conference Time!</h1>
                        <form className={formClasses}  onSubmit={handleSubmit} id="create-location-form">

                            <div className={spinnerClasses} id="loading-conference-spinner">
                                <div className="spinner-grow text-secondary" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div>

                            <div className="mb-3">
                                <div className="form-label">Please choose which conference you'd like to attend.</div>
                                <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className={dropdownClasses} >
                                    <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option key={conference.href} value={conference.href}>
                                                {conference.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>

                            <div className="row g-2 mb-3">
                                <div className="form-label">Now, tell us about yourself.</div>
                                <div className="col-md">
                                    <div className="form-floating">
                                        <input onChange={handleNameChange} value={name} type="text" className="form-control" id="name" placeholder="Your full name" />
                                        <label htmlFor="name">Your full name</label>
                                    </div>
                                </div>
                                <div className="col-md">
                                    <div className="form-floating">
                                        <input onChange={handleEmailChange} value={email} type="email" className="form-control" id="email" placeholder="Your email address" />
                                        <label htmlFor="email">Your email address</label>
                                    </div>
                                </div>
                            </div>

                            <button className="btn btn-primary">I'm going!</button>
                        </form>

                        <div className={messageClasses} id="success-message">
                            Congratulations! You're all signed up!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AttendConferenceForm;
