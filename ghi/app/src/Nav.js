import { NavLink } from "react-router-dom";

function Nav() {
    return (
        <div className="container-fluid">
            <div className="navbar navbar-expand-lg navbar-light bg-light" id="navbarSupportedContent">
                <NavLink className="navbar-brand" to="/">Conference GO!</NavLink>
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/">Main Page</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/attendees">Attendee List</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/attendees/new">New Attendee</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/locations/new">New Location</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" aria-current="page" to="/presentations/new">New Presentation</NavLink>
                    </li>
                </ul>
            </div>
        </div>
    );
  }

  export default Nav;
